package test;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.RingList;
import model.logic.STSManager;

public class STSManagerTest extends TestCase {

	private STSManager stsManager;
	
	protected void setUp() throws Exception {
		stsManager = new STSManager();
	}

	public void testLoadRoutes() throws Exception {
		stsManager.loadRoutes("./data/test/routes_test.txt");

		assertEquals("Se cargaron los elementos del archivo Routes", stsManager.getRoutes().getSize().intValue(), 246);
	}
	
	public void testLoadTrips() throws Exception {
		stsManager.loadTrips("./data/test/trips_test.txt");

		assertEquals("Se cargaron los elementos del archivo Trips", stsManager.getTrips().getSize().intValue(), 1050);
	}
	
	public void testLoadStopTimes() throws Exception {
		stsManager.loadStopTimes("./data/test/stop_times_test.txt");

		assertEquals("Se cargaron los elementos del archivo StopTimes", stsManager.getStopTimes().getSize().intValue(), 2283);
	}

	public void testLoadStops() throws Exception {
		stsManager.loadStops("./data/test/stops_test.txt");

		assertEquals("Se cargaron los elementos del archivo Stops", stsManager.getStops().getSize().intValue(), 1009);
	}
}
