package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	private DoubleLinkedList<VORoute> routes;
	private DoubleLinkedList<VOStopTime> stopTimes;

	private RingList<VOTrip> trips;
	private RingList<VOStop> stops;


	public STSManager() {
		routes= new DoubleLinkedList<VORoute>();
		stopTimes=  new DoubleLinkedList<VOStopTime>();

		trips= new  RingList<VOTrip>();
		stops=new  RingList<VOStop>();
	}

	public DoubleLinkedList<VORoute> getRoutes()
	{
		return routes;
	}

	public DoubleLinkedList<VOStopTime> getStopTimes()
	{
		return stopTimes;
	}

	public RingList<VOTrip> getTrips()
	{
		return trips ;
	}

	public RingList<VOStop> getStops()
	{
		return stops ;
	}

	@Override
	public void loadRoutes(String routesFile) {
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try 
		{
			file= new FileReader(routesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String dato[] = cadena.split(",");

					int routeId = Integer.parseInt(dato[0].trim());
					String agencyId = dato[1].trim();
					String routeShortName = dato[2].trim();
					String routeLongName = dato[3].trim();
					String routeDesc = dato[4].trim();
					int routeType = Integer.parseInt(dato[5].trim());
					String routeUrl = dato[6].trim();
					Integer routeColor = dato[7].trim().isEmpty() ? null : Integer.parseInt(dato[7].trim());
					String routeTextColor = dato[8].trim();

					VORoute newRoute = new VORoute(routeId, agencyId, routeShortName, routeLongName, routeDesc, routeType, routeUrl, routeColor, routeTextColor);

					routes.addAtEnd(newRoute); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
	}

	@Override
	public void loadTrips(String tripsFile) {
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(tripsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String dato[] = cadena.split(",");

					int routeId = Integer.parseInt(dato[0].trim());
					int serviceId = Integer.parseInt(dato[1].trim());
					int tripId = Integer.parseInt(dato[2].trim());
					String tripHeadsign = dato[3].trim();
					String tripShortName = dato[4].trim();
					int directionId = Integer.parseInt(dato[5].trim());
					int blockId = Integer.parseInt(dato[6].trim());
					int shapeId = Integer.parseInt(dato[7].trim());
					int wheelchairAccessible = Integer.parseInt(dato[8].trim());
					int bikesAllowed = Integer.parseInt(dato[9].trim());

					VOTrip newTrip = new VOTrip(routeId, serviceId, tripId, tripHeadsign, tripShortName, directionId, blockId, shapeId, wheelchairAccessible, bikesAllowed);

					trips.addAtEnd(newTrip); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = datos[1].trim();
					String departureTime = datos[2].trim();
					int stopId = Integer.parseInt(datos[3].trim());
					int stopSequence = Integer.parseInt(datos[4].trim());
					Integer stopHeadsign = datos[5].trim().isEmpty() ? null : Integer.parseInt(datos[5].trim());
					int pickupType = Integer.parseInt(datos[6].trim());
					int dropOffType = Integer.parseInt(datos[7].trim());
					Double shapeDistTraveled = null;
					if(datos.length > 8)
					{
						shapeDistTraveled = datos[8].trim().isEmpty() ? null : Double.parseDouble(datos[8].trim());
					}

					VOStopTime newStopTime = new VOStopTime(tripId, arrivalTime, departureTime, stopId, stopSequence, stopHeadsign, pickupType, dropOffType, shapeDistTraveled);

					stopTimes.addAtEnd(newStopTime); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
	}

	@Override
	public void loadStops(String stopsFile) {
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;
		String datos[] = null;

		try
		{
			file= new FileReader(stopsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int stopId = Integer.parseInt(datos[0].trim());
					Integer stopCode = datos[1].trim().isEmpty() ? null : Integer.parseInt(datos[1].trim());
					String stopName = datos[2].trim();
					String stopDesc = datos[3].trim();
					double stopLat = Double.parseDouble(datos[4].trim());
					double stopLon = Double.parseDouble(datos[5].trim());
					String zoneId = datos[6].trim();
					String stopUrl = datos[7].trim();
					Integer locationType = datos[8].trim().isEmpty() ? null : Integer.parseInt(datos[8].trim());
					String parentStation = "";
					if(datos.length > 9)
					{
						parentStation = datos[9].trim();
					}

					VOStop newStop = new VOStop(stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType, parentStation);

					stops.addInOrder(newStop);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
	}

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		DoubleLinkedList<VORoute> r = new DoubleLinkedList<>();
		boolean e= false;


		Iterator<VOStop> it= stops.iterator();
		while(it.hasNext() && !e) {
			VOStop n = it.next();
			if(n.getName().equals(stopName)) {
				Iterator<VOStopTime> itt= stopTimes.iterator();
				while(itt.hasNext()) {
					VOStopTime m= itt.next();
					if(m.getStopId()==n.id()) {
						Iterator<VOTrip> ittt= trips.iterator();
						while(ittt.hasNext()){
							VOTrip o= ittt.next();
							if(o.id()==m.getTripId()) {
								Iterator<VORoute> i4t= routes.iterator();
								while(i4t.hasNext()) {
									VORoute p= i4t.next();
									if(o.getRouteId()==p.id()) {
										if(!r.existElement(p)) {
											r.addAtEnd(p);
										}
									}
								}
							}
						}





					}





				}



				e=true;	
			}

		}

		return r;
	}


	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		try
		{
			int dir = direction.trim().isEmpty() ? null : Integer.parseInt(direction);
			DoubleLinkedList<VOStop> r = new DoubleLinkedList<>();
			boolean e= false;
			Iterator<VORoute> it= routes.iterator();
			while (it.hasNext() && !e) {
				VORoute q= it.next();
				if(q.getName().compareTo(routeName)==0) {
					Iterator<VOTrip> itt= trips.iterator();
					while(itt.hasNext()) {
						VOTrip w= itt.next();
						if(w.getDirectionId()==dir) {
							Iterator<VOStopTime> i3t= stopTimes.iterator();
							while(i3t.hasNext()) {
								VOStopTime t= i3t.next();
								if(t.getTripId()==w.id()) {
									Iterator<VOStop> i4t= stops.iterator();
									while(i4t.hasNext()) {
										VOStop y= i4t.next();
										if(t.getStopId()==y.id()) {
											if(!r.existElement(y)) {
												r.add(y);
											}
										}

									}
								}
							}
						}
					}
					e=true;
				}
			}

			return r;
		}
		catch(Exception e)
		{
			throw new RuntimeException("Error al calcular las paradas. " + e.getMessage(), e);
		}
	}

}
