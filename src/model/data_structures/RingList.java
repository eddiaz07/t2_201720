package model.data_structures;

import java.util.Iterator;

public class RingList<T extends Comparable<T> > implements IList<T> 

{

	private NodoListaDoblementeEncadenada<T> nodoActual;
	private int size;

	/**
	 * Construye una lista circular vacia
	 */
	public RingList() 
	{
		nodoActual = new NodoListaDoblementeEncadenada<T>(null);
		size = 0;
	}

	/**
	 * Construye una lista circular con un solo nodo
	 */

	public RingList(NodoListaDoblementeEncadenada<T> nodo) 
	{
		nodoActual = nodo;
		nodoActual.cambiarAnterior(nodoActual);
		nodoActual.cambiarSiguiente(nodoActual);	
		size = 1;
	}


	/**
	 * Devuelve un iterador sobre la lista;
	 */
	public Iterator<T> iterator() {

		Iterator<T> iterador = new RingListIterator();

		return iterador;
	}

	/**
	 * Devuelve el tamaño de la lista;
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * cambia la referencia del nodo actual por el siguiente.
	 */
	public void next() 
	{
		nodoActual = nodoActual.darSiguiente();


	}

	/**
	 * cambia la referencia del nodo actual por el anterior.
	 */

	public void previous() {
		nodoActual = nodoActual.darAnterior();

	}

	/**
	 * devuelve el elemento del nodo actual
	 */


	public T getElement() {

		return nodoActual.darElemento();
	}

	public boolean existElement(T element) {
		boolean existe = false;
		if(size != 0)
		{
			NodoListaDoblementeEncadenada<T> temp = nodoActual;
			int i = 0;
			while(i < size && !existe)
			{
				if(temp.darElemento().compareTo(element) == 0)
				{
					existe = true;
				}
				temp = temp.darSiguiente();
				i++;
			}
		}

		return existe;
	}



	/**
	 * Devuelve el elemento del nodo que se encuentra en la posicion K
	 * @param k la posición del nodo del que se desea el elemento.
	 * @return el elemento  posición
	 * @throws Exception si pos < 0 o pos >= size() 
	 * @throws Exception si la lista esta vacia
	 */

	@Override
	public T getElementAtK(int k) throws Exception {
		T elemento = null;
		NodoListaDoblementeEncadenada<T> temp = nodoActual;
		if(nodoActual.darElemento() == null)
		{
			throw new Exception("La lista esta vacia");
		}
		else if( k > size-1)
		{
			int p = k % size;
			elemento = getElementAtK(p);
		}
		else
		{ int i = 0;
		while(i < k)
		{
			i++;
			temp = temp.darSiguiente();
		}

		elemento = temp.darElemento();

		}

		return elemento;
	}

	/**
	 * agrega un nuevo nodo a la lista reemplazando al nodo actual como primer nodo.
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */

	public boolean add(T elementToAdd) throws Exception
	{
		boolean add = false;
		NodoListaDoblementeEncadenada<T> nodo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		if (elementToAdd == null)
		{
			throw new NullPointerException("el elemento es nulo");		
		}
		if(size == 0)
		{	
			nodoActual = nodo;
			nodoActual.cambiarSiguiente(nodoActual);
			nodoActual.cambiarAnterior(nodoActual);
			add = true;
			size++;

		}
		else
		{
			NodoListaDoblementeEncadenada<T> temp = nodoActual.darAnterior();
			temp.cambiarSiguiente(nodo);
			nodo.cambiarAnterior(temp);
			nodo.cambiarSiguiente(nodoActual);
			nodoActual.cambiarAnterior(nodo);
			size++;
			nodoActual = nodo;
			add = true;

		}


		return add;
	}

	/**
	 * agrega un nuevo nodo a la lista en orden
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */
	public boolean addInOrder(T elementToAdd) throws NullPointerException
	{
		boolean add = false;
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);


		if (elementToAdd == null)
		{
			throw new NullPointerException("el elemento es nulo");

		}

		else if(size == 0)
		{
			nodoActual = nuevo;
			nodoActual.cambiarSiguiente(nodoActual);
			nodoActual.cambiarAnterior(nodoActual);
			add = true;
			size++;
		}
		else
		{
			int i = 0;
			NodoListaDoblementeEncadenada<T> temp = nodoActual;
			while(temp.darElemento().compareTo(elementToAdd) < 0 && i < size )
			{
				temp = temp.darSiguiente();

				i++;
			}
			NodoListaDoblementeEncadenada<T> temp2 = temp;
			nuevo.cambiarAnterior(temp.darAnterior());
			nuevo.cambiarSiguiente(temp);
			temp.darAnterior().cambiarSiguiente(nuevo);
			temp.cambiarAnterior(nuevo);

			add = true;
			size++;
		}

		return add;
	}

	/**
	 * agrega un nuevo nodo a la lista en la ultima posicion de la lista.
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */
	public boolean addAtEnd(T elementToAdd) throws NullPointerException
	{
		boolean add = false;
		if (elementToAdd == null)
		{
			throw new NullPointerException("el elemento es nulo");

		}
		NodoListaDoblementeEncadenada<T> temp = nodoActual;
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		if(size == 0)
		{
			nodoActual = nuevo;
			nodoActual.cambiarSiguiente(nodoActual);
			nodoActual.cambiarAnterior(nodoActual);
			add = true;
			size++;
		}
		else
		{
			int i = 0;
			while(i < size )
			{
				temp = temp.darSiguiente();

				i++;
			}
			NodoListaDoblementeEncadenada<T> temp2 = temp.darAnterior();

			nuevo.cambiarAnterior(temp2);
			nuevo.cambiarSiguiente(nodoActual);

			temp2.cambiarSiguiente(nuevo);
			nodoActual.cambiarAnterior(nuevo);

			add = true;
			size++;
		}



		return add;

	}

	/**
	 * agrega un nuevo nodo en la posicion indicada por parametro
	 * @param elementToAdd el elemento a añadir
	 * @param k la posicion en la que se desa añadir el elemento
	 * @return true si se añadio el elemento, false en caso de que k este fuera de los parametos
	 * @throws Exception si el elemento es nulo
	 */

	public boolean addAtK(T elementToAdd, int k) throws Exception 
	{
		boolean add = false;
		if (elementToAdd == null)
		{
			throw new Exception("el elemento es nulo");
		}
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		NodoListaDoblementeEncadenada<T> temp = nodoActual;
		if(getSize() == 0 && k == 0)
		{
			nodoActual = nuevo;
			nodoActual.cambiarAnterior(nodoActual);
			nodoActual.cambiarSiguiente(nodoActual);
			size++;
		}
		else if( k > getSize())
		{
			int p = k % size;
			add = deleteAtK(p);
		}
		
		else
		{ int i = 0;

		while(i < k)
		{
			i++;
			temp = temp.darSiguiente();
		}

		NodoListaDoblementeEncadenada<T> temp2 = temp.darAnterior();
		nuevo.cambiarSiguiente(temp);
		temp2.cambiarSiguiente(nuevo);
		temp.cambiarAnterior(nuevo);
		nuevo.cambiarAnterior(temp2);
		size++;
		add = true;
		}

		return add;
	}


	/**
	 * Elimina el nodo actual
	 * @return true si el elemento fue eliminado
	 * @return false si la lista esta vacia
	 */

	public boolean delete() 
	{
		boolean delete = true;

		if(size == 0)
		{
			delete = false;
		}

		else if (size == 1)
		{
			nodoActual = null;
			size--;
		}
		else
		{
			NodoListaDoblementeEncadenada<T>temp = nodoActual.darSiguiente();
			nodoActual.darAnterior().cambiarSiguiente(temp);
			temp.cambiarAnterior(nodoActual.darAnterior());
			size--;
			nodoActual = temp;


		}


		return delete;
	}

	/**
	 * elimina el nodo que tenga el mismo elemento que el dado por parametro
	 * @param elemento a eliminar.
	 * @return false si no se elimino el nodo
	 * @return true si se elimino el nodo
	 */

	public boolean deleteElement(T elementToDelete) 
	{

		boolean delete = false;
		NodoListaDoblementeEncadenada<T> temp = nodoActual;
		if(getSize() == 0)
		{
			delete = false;
		}
		else if(getElement().compareTo(elementToDelete) == 0 && size == 1)
		{
			delete = delete();
		}
		else
		{ 
			int i = 0;
			while(i < size  && !delete) 
			{
				if(elementToDelete.compareTo(temp.darElemento()) == 0)
				{
					NodoListaDoblementeEncadenada<T> temp1 = temp;
					temp.darAnterior().cambiarSiguiente(temp.darSiguiente());
					temp.darSiguiente().cambiarAnterior(temp1.darAnterior());

					delete = true;
					size--;
				}
				else{
					i++;
					temp = temp.darSiguiente();
				}
			}


		}

		return delete;
	}

	/**
	 * elimina el nodo que se encuentra en la posicion K
	 * @param k la posición del nodo del que se desea el elemento.
	 * @return true si el elemento fue eliminado;
	 * @return false si el elemento no existe o la lista esta vacia;
	 */

	public boolean deleteAtK(int k) {
		boolean delete = true;
		NodoListaDoblementeEncadenada<T> temp = nodoActual;
		if(getSize() == 0 )
		{
			delete = false;
		}
		else if( k > getSize())
		{
			int p = k % size;
			delete = deleteAtK(p);
			
		}
		else if(k == 0 && size == 1)
		{
			size--;
			nodoActual = null;

		}
		else
		{ int i = 0;
		while(i < k)
		{
			i++;
			temp = temp.darSiguiente();
		}

		temp.darSiguiente().cambiarAnterior(temp.darAnterior());
		temp.darAnterior().cambiarSiguiente(temp.darSiguiente());
		size--;

		}

		return delete;
	}

	public class RingListIterator implements Iterator<T>
	{
		NodoListaDoblementeEncadenada<T> anterior;
		NodoListaDoblementeEncadenada<T> siguiente;

		public RingListIterator() 
		{
			anterior = nodoActual.darAnterior();
			siguiente = nodoActual;
		}

		/**
		 * @return true si no se ha llegado al final de la lista;
		 * @return false si se llego al final de la lista;
		 */
		public boolean hasNext() 
		{
			return siguiente != nodoActual;
		}

		/**
		 * devuelve el elemento del siguiente nodo
		 */
		public T next() {

			anterior = siguiente;
			siguiente = siguiente.darSiguiente();


			return anterior.darElemento();
		}



	}




}
